﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kellogg.Registration.Common;
//using Kellogg.Registration.Redistributable;


namespace prueba2.Controllers
{
    
    public class HomeController : Controller
    {
        String ModuleKey = "2783353f-0fbb-46e4-9f37-44c59dde1e33";
        String SharedSecretKey = "3fdb6ac4-e152-4b94-9163-5b357f8a1aae";
        String AESKey = "iODauf5bj3iVDgSPHPW2FEmqkhJXYyqkakXijGlJ0yI=";
        String AESInitializationVector = "cLAgqQww/jvzA+nzNObYcw==";
        public ActionResult Index()
        {
            String date = DateTime.UtcNow.ToString("yyyy-mm-ddThh:mm:ssZ");
            String EncryptedSharedSecret = Test();
            //String encode = date + "|" + EncryptedSharedSecret;
            //Dictionary < String, String> ParamsAsDictionary;          
            //ParamsAsDictionary = PostAuthorizationParams.Decrypt(EncryptedSharedSecret, AESKey, AESInitializationVector);
            ViewBag.EncryptedSharedSecret = EncryptedSharedSecret;
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public String Test()
        {
            String EncryptedSharedSecret;
            EncryptedSharedSecret = SharedSecret.Generate(SharedSecretKey, AESKey, AESInitializationVector);      
            return EncryptedSharedSecret;
        }
    }
}